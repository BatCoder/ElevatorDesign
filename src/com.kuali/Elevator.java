package com.kuali;

import java.util.Set;
import com.kuali.ElevatorController; 

public class Elevator {
	private int elevatorNum;
	private int currFloor;
	private int direction;	//-1 going downwards; 1 going upwards; 0 is stopped
	private int destination;
	private int numberOfTrips=0;
	private int numberOfFloorsPassed=0;
	private boolean onMaintenanceHold=false;
	private Set<Floor> openAt;

	public int getCurrFloor() {
		return currFloor;
	}

	public void setCurrFloor(int currFloor) {
		this.currFloor = currFloor;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public int getElevatorNum() {
		return elevatorNum;
	}

	public void setElevatorNum(int elevatorNum) {
		this.elevatorNum = elevatorNum;
	}

	public int getDestination() {
		return destination;
	}

	public void setDestination(int destination) {
		this.destination = destination;
		openAt.add(new Floor(destination));
	}
	
	public void reportCurrentFloor() {
		System.out.println("Currently at: " + currFloor);
		
		for(Floor floor: openAt)
			if(floor.getFloorNumber()==currFloor)
				openAt.remove(floor);
	}
	
	public void reportIfOpen() {
		System.out.println("Door opened");
	}
	
	public void summonElevator(int floorNumber) {
		openAt.add(new Floor(floorNumber));
	}
	
	public void move() {
		if(direction==0) return;
		else if(direction==1) moveUp();
		else moveDown();
	}
	
	public boolean isOnMaintenanceHold() {
		return onMaintenanceHold;
	}

	public void setOnMaintenanceHold(boolean onMaintenanceHold) {
		this.onMaintenanceHold = onMaintenanceHold;
	}
	
	public void moveUp() {
		ElevatorController elevatorController = new ElevatorController();
		if(currFloor==elevatorController.getNumberOfFloors()) {
			numberOfTrips++;
			if(numberOfTrips==100) setOnMaintenanceHold(true);
			return;		//the elevator cannot go above this floor, so no currFloor++
		}
		
		currFloor++;
	}
	
	public void moveDown() {
		ElevatorController elevatorController = new ElevatorController();
		if(elevatorController.getGroundFloor()==currFloor) return;	//cannot go past the ground floor
		currFloor--;
	}
}
