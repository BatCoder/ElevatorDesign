package com.kuali;

import java.util.HashSet;
import java.util.Set;
import com.kuali.Elevator;

public class ElevatorController {
	private int numberOfElevators;
	private int numberOfFloors;
	private final int groundFloor=1;
	public int getGroundFloor() {
		return groundFloor;
	}

	Set<Elevator> unoccupied;		  //set of unoccupied elevators
	Set<Elevator> setOfElevators;	//set of all the elevators

	public int getNumberOfFloors() {
		return numberOfFloors;
	}

	public void setNumberOfFloors(int numberOfFloors) {
		this.numberOfFloors = numberOfFloors;
	}	
	
	public ElevatorController() {
		super();
	}
	
	public ElevatorController(int numberOfElevators, int numberOfFloors) {
		super();
		this.numberOfElevators = numberOfElevators;
		this.setNumberOfFloors(numberOfFloors);
		
		setOfElevators = new HashSet<Elevator>();
		unoccupied = new HashSet<Elevator>();
		for(int i=1; i<numberOfElevators; i++) {
			Elevator elevator=new Elevator();
			elevator.setElevatorNum(i);
			elevator.setCurrFloor(groundFloor);
			elevator.setDirection(0);
			
			setOfElevators.add(elevator);
			unoccupied.add(elevator);
		}
	}

	void getElevator(int floorNumber) {
		if(floorNumber<groundFloor || floorNumber>getNumberOfFloors()) return;
		
		//O(n) to determine if an unoccupied working elevator is already stopped at the requested floor
		for(Elevator elevator: unoccupied)
			if(elevator.getCurrFloor()==floorNumber && elevator.isOnMaintenanceHold()==false) {
				unoccupied.remove(elevator);
				elevator.summonElevator(floorNumber);
			}
		
		//O(n) to determine if an occupied working elevator is moving and will pass that floor on its way
		for(Elevator elevator: setOfElevators)
			if(!unoccupied.contains(elevator) && (elevator.getCurrFloor()<floorNumber && elevator.getDirection()==-1)
					||(elevator.getCurrFloor()>floorNumber && elevator.getDirection()==1)
					&& elevator.isOnMaintenanceHold()==false) {
				elevator.summonElevator(floorNumber);
			}
			
		//If we didn't find an elevator yet, then return the closest unoccupied working elevator in O(n^2)
		int i=1;
		while((floorNumber+i)<=getNumberOfFloors() || (floorNumber-i)>=groundFloor) {
			for(Elevator elevator: unoccupied) {
				if(((elevator.getElevatorNum()==floorNumber+i) && elevator.isOnMaintenanceHold()==false) || 
						((elevator.getElevatorNum()==floorNumber-i) && elevator.isOnMaintenanceHold()==false)) {
					unoccupied.remove(elevator);
					elevator.summonElevator(floorNumber);
				}
			}
			i++;
		}
	}
}
