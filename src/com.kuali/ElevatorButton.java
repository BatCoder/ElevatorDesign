package com.kuali;

import com.kuali.Elevator;

public class ElevatorButton {
	private int destination;
	
	public ElevatorButton(int destination) {
		super();
		this.destination = destination;
	}

	public void buttonPressed() {
		Elevator elevator = new Elevator();
		elevator.setDestination(destination);
	}
}
