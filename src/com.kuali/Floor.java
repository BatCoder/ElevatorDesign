package com.kuali;

import com.kuali.ElevatorController;

public class Floor {
	private int floorNumber;
	
	public int getFloorNumber() {
		return floorNumber;
	}

	public void setFloorNumber(int floorNumber) {
		this.floorNumber = floorNumber;
	}

	public Floor(int floorNumber) {
		super();
		this.floorNumber = floorNumber;
	}
	
	public void buttonPressed() {
		ElevatorController elevatorController = new ElevatorController();
		elevatorController.getElevator(floorNumber);
	}
}
